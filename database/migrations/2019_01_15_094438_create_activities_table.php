<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('name',255);
            $table->dateTime('start_date');
            $table->dateTime('end_date');
            $table->integer('id_project')->unsigned();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_project', 'fk_id_project1')->references('id')->on('projects');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->dropForeign('fk_id_project1');
        });

        Schema::dropIfExists('activities');
    }
}

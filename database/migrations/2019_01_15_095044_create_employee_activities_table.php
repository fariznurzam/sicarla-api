<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_employee', function (Blueprint $table) {
            $table->integer('id_employee')->unsigned();
            $table->integer('id_activity')->unsigned();
            $table->tinyinteger('status');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('id_employee', 'fk_id_employee2')->references('nip')->on('employees');
            $table->foreign('id_activity', 'fk_id_activity1')->references('id')->on('activities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity_employee', function (Blueprint $table) {
            $table->dropForeign('fk_id_employee2');
            $table->dropForeign('fk_id_activity1');
        });

        Schema::dropIfExists('activity_employee');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('score');
            $table->string('desc',255)->nullable()->default(null);
            $table->integer('id_employee_sender')->unsigned();
            $table->integer('id_employee_receiver')->unsigned();
            $table->integer('id_activity')->unsigned();
            $table->foreign('id_employee_sender', 'fk_id_employee_sender')->references('nip')->on('employees');
            $table->foreign('id_employee_receiver', 'fk_id_employee_receiver')->references('nip')->on('employees');
            $table->foreign('id_activity', 'fk_id_activity_table_ratings')->references('nip')->on('employees');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ratings', function (Blueprint $table) {
            $table->dropForeign('fk_id_employee_sender');
            $table->dropForeign('fk_id_employee_receiver');
            $table->dropForeign('fk_id_activity_table_ratings');
        });

        Schema::dropIfExists('ratings');
    }
}

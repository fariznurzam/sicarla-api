<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Employee::class, function (Faker\Generator $faker) {
    $faker->addProvider(new Faker\Provider\id_ID\Person($faker));

    return [
        'nip' => $faker->numberBetween(100, 999),
        'name' => $faker->name,
        'rating' => $faker->numberBetween(1, 5)
    ];
});

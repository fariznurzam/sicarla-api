<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        $this->call('EmployeesTableSeeder');
        $this->call('ProjectsTableSeeder');
        $this->call('ActivitiesTableSeeder');
        $this->call('EmployeeActivitiesTableSeeder');
        $this->call('RatingsTableSeeder');
    }
}

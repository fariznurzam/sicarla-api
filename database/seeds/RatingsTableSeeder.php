<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class RatingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//$this->disableForeignKeys();
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('ratings')->truncate();
        //$this->truncate('ratings');

        $rating = [
            [
                'score' => 5,
                'desc' => 'works well',
                'id_employee_receiver' => App\Employee::inRandomOrder()->first()->nip,
                'id_employee_sender' => App\Employee::inRandomOrder()->first()->nip,
                'id_activity' => App\Activity::inRandomOrder()->first()->id,
            ],
            [
                'score' => 3,
                'desc' => null,
                'id_employee_receiver' => App\Employee::inRandomOrder()->first()->nip,
                'id_employee_sender' => App\Employee::inRandomOrder()->first()->nip,
                'id_activity' => App\Activity::inRandomOrder()->first()->id,
            ],
        ];

        DB::table('ratings')->insert($rating);

        //$this->enableForeignKeys();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        //
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//$this->disableForeignKeys();
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('activities')->truncate();
        //$this->truncate('activities');

        $activity = [
            [
                'code' => 'BWP1700601',
                'name' => 'Notional Pooling System',
                'start_date' => date("2018-11-26 19:21:52"),
                'end_date' => date("2018-11-28 11:25:22"),
                'id_project' => 1,
            ],
            [
                'code' => 'DSEDM16101',
                'name' => 'Enhancement ETL dan Datamart',
                'start_date' => date("2018-11-27 07:09:11"),
                'end_date' => date("2018-11-29 16:22:05"),
                'id_project' => 2,
            ],
        ];

        DB::table('activities')->insert($activity);

        //$this->enableForeignKeys();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        //
    }
}
<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::disableForeignKeyCheck();
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('projects')->truncate();
        // $this->truncate('projects');

        $project = [
            [
            	'code' => 'DBF1700207',
                'name' => 'Branchless Banking',
                'desc' => 'Smart EDC, sebagai agen branchless banking untuk melakukan pembukaan rekenning, transaksi tarik tunai, serta jasa penerima pembayaran utilitas. Dibutuhkan orang bagian Channel',
                'id_employee' => App\Employee::inRandomOrder()->first()->nip,
                'start_date' => date("2018-11-19 11:22:34"),
                'end_date' => date("2018-11-30 19:31:12"),
            ],
            [
            	'code' => 'IFS1700900',
                'name' => 'Advanced Cash Management',
                'desc' => null,
                'id_employee' => App\Employee::inRandomOrder()->first()->nip,
                'start_date' => date("2018-11-20 21:30:04"),
                'end_date' => date("2018-11-28 09:41:55"),
            ],
            [
            	'code' => 'IFS1700940',
                'name' => 'eCash New Core System',
                'desc' => null,
                'id_employee' => App\Employee::inRandomOrder()->first()->nip,
                'start_date' => date("2018-11-19 11:22:34"),
                'end_date' => date("2018-11-29 19:31:12"),
            ]
        ];

        // $employees = factory(App\Project::class, 10)->create();

        DB::table('projects')->insert($project);

        // $this->enableForeignKeys();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        //
    }
}
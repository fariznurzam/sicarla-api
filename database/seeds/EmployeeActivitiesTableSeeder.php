<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class EmployeeActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	//$this->disableForeignKeys();
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        DB::table('activity_employee')->truncate();
        //$this->truncate('employee_activities');

        $employee_activity = [
            [
                'id_employee' => App\Employee::inRandomOrder()->first()->nip,
                'id_activity' => 1,
                'status' => 1,
            ],
            [
                'id_employee' => App\Employee::inRandomOrder()->first()->nip,
                'id_activity' => 1,
                'status' => 1,
            ],
            [
                'id_employee' => App\Employee::inRandomOrder()->first()->nip,
                'id_activity' => 2,
                'status' => 1,
            ],
        ];

        DB::table('activity_employee')->insert($employee_activity);

        //$this->enableForeignKeys();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        //
    }
}

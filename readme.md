# Sicarla API

Sicarla API is a microservice based on Sicarla Project, a human resource allocation application for various IT projects. Build based by Lumen microservice framework.

## Requirements
1. PHP 7.0
2. MySQL
3. Composer [Get it here](https://getcomposer.org/)

## How to install
- Open Command prompt or Terminal
- ` git clone https://fariznurzam@bitbucket.org/fariznurzam/sicarla-api.git `
- ` cd sicarla-api `
- Copy .env.example into .env
- Change these 
```
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret

CACHE_DRIVER=file
QUEUE_CONNECTION=sync
```
into 
```
DB_DATABASE=your_database
DB_USERNAME=your_username
DB_PASSWORD=your_password

CACHE_DRIVER=array
QUEUE_CONNECTION=database

SWAGGER_VERSION=2.0
```
- ` composer install ` to install supporting depencies / packages
- ` php artisan migrate:refresh --seed ` to generate database and dump the seed (make sure, you already create the database in MySQL)
- ` php artisan swagger-lume:publish ` to publish setting of API documentation
- ` php artisan swagger-lume:generate ` to generation UI API documentation (if there is update of API documentation annotation, please use this command to re-generate it)
- ` php -S localhost:8000 public/index.php ` to run the project
- visit ` localhost:8000 `
- visit ` localhost:8000/api/documentation ` to see API documentation

## Unit Test
Unit test able to simulately test the whole application process easily, but it's optional.
- Already did `composer install`
- Open phpunit.xml file
```angular2html
        <env name="QUEUE_CONNECTION" value="sync"/>
```
to
```angular2html
        <env name="QUEUE_CONNECTION" value="sync"/>
        <env name="DB_DATABASE" value="your_database_test"/>
```
- Create database with name as your DB_DATABASE value in phpunit.xml
- Run `env/bin/phpunit`

# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://poser.pugx.org/laravel/lumen-framework/d/total.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/lumen-framework/v/stable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/lumen-framework/v/unstable.svg)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://poser.pugx.org/laravel/lumen-framework/license.svg)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Official Documentation

Documentation for the framework can be found on the [Lumen website](https://lumen.laravel.com/docs).

## Security Vulnerabilities

If you discover a security vulnerability within Lumen, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Lumen framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

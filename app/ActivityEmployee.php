<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityEmployee extends Model
{
    use SoftDeletes;

    protected $table = 'activity_employee';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_employee', 'id_activity', 'status'
    ];
}

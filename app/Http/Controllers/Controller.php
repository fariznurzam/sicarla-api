<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;

/**
 * @SWG\Swagger(
 *     basePath="/api/v1/",
 *     schemes={"http"},
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Sicarla API Microservice",
 *         description="Documentation by Swagger",
 *         @SWG\Contact(
 *             email="t90fariz@gmail.com"
 *         ),
 *     )
 * )
 */
class Controller extends BaseController
{
}

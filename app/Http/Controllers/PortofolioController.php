<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Employee;
use App\Project;
use App\ActivityEmployee;
use Illuminate\Http\Request;

class PortofolioController extends Controller
{
	/**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

    /**
     * @SWG\Get(
     *   path="/portofolio/{id}",
     *   summary="Return portofolio of employee",
     *   tags={"Portofolio"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="Employee ID",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="OK"
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="NOT FOUND"
     *   )
     * )
     */
    public function show($nip)
    {
        $employee = Employee::where('nip','=', $nip)
            ->whereNull('deleted_at')
            ->firstOrFail();

        $data = $employee->activities()->get();

        $result = [
            'message' => 'Success',
            'count' => $data->count(),
            'data' => $data,
        ];

        return response()->json($result, 200);
    }
}

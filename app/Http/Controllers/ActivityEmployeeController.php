<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Employee;
use App\Project;
use App\ActivityEmployee;
use Illuminate\Http\Request;

class ActivityEmployeeController extends Controller
{
	/**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

    public function index()
    {
        $activities_employee = ActivityEmployee::whereNull('deleted_at')->get();

		foreach ($activities_employee as $i => $activity_employee) {
			$data[$i] = $activity_employee;
		}

        $result = [
            'message' => 'Success',
            'count' => count($data),
            'data' => $data,
        ];

        return response()->json($result, 200);
    }

    public function show($id)
    {
        $activity_employee = ActivityEmployee::where('id_employee','=', $id)
            ->whereNull('deleted_at')
            ->firstOrFail();

        $data = $activity_employee;

        $result = [
            'message' => 'Success',
            'data' => $data,
        ];

        return response()->json($result, 200);
    }
}
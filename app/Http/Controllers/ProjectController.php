<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Employee;
use App\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
	/**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

	/**
     * @SWG\Get(
     *   path="/projects",
     *   summary="Return a list of projects",
     *   tags={"Project"},
     *   @SWG\Response(
     *     response=200,
     *     description="OK"
     *   )
     * )
     */
    public function index()
    {
        $projects = Project::whereNull('deleted_at')->get();

        $result = [
            'message' => 'Success',
            'count' => $projects->count(),
            'data' => $projects,
        ];

        return response()->json($result, 200);
    }

    /**
     * @SWG\Get(
     *   path="/project/{id}",
     *   summary="Return a project",
     *   tags={"Project"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="project ID",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="OK"
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="NOT FOUND"
     *   )
     * )
     */
    public function show($id)
    {
        $project = Project::where('id','=', $id)
            ->whereNull('deleted_at')
            ->firstOrFail();

        $data = $project;

        $result = [
            'message' => 'Success',
            'data' => $data,
        ];

        return response()->json($result, 200);
    }

    /**
     * @SWG\Post(
     *   path="/project",
     *   summary="Create new project",
     *   tags={"Project"},
     *   @SWG\Parameter(
     *     name="code",
     *     in="formData",
     *     description="project code",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="project name",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="desc",
     *     in="formData",
     *     description="project description",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="id_employee",
     *     in="formData",
     *     description="employee ID",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="start_date",
     *     in="formData",
     *     description="Start project date",
     *     required=true,
     *     default="2018-12-30 12:30:00",
     *     format="date-time",
     *     type="string",
     *   ),
     *   @SWG\Parameter(
     *     name="end_date",
     *     in="formData",
     *     description="End project date",
     *     required=true,
     *     default="2018-12-30 12:30:00",
     *     format="date-time",
     *     type="string",
     *   ),
     *   @SWG\Response(
     *     response=201,
     *     description="CREATED"
     *   )
     * )
     */
    public function create(Request $request)
    {
        $project = new Project();

        $project->code = $request->code;
        $project->name = $request->name;
        $project->desc = $request->desc;
        $project->start_date = $request->start_date;
        $project->end_date = $request->end_date;
        $project->id_employee = $request->id_employee;

        $project->save();

        $result = [
            'message' => 'Created',
        ];

        return response()->json($result, 201);
    }

}
<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Rating;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

    /**
     * @SWG\Get(
     *   path="/employees",
     *   summary="Return a list of employees",
     *   tags={"Employee"},
     *   @SWG\Response(
     *     response=200,
     *     description="OK"
     *   )
     * )
     */
    public function index()
    {
        $employees = Employee::whereNull('deleted_at')->get();

        foreach ($employees as $i => $employee) {
            $employeeRatings = Rating::where('id_employee_receiver', '=', $employee->nip)
                ->whereNull('deleted_at')
                ->get();

            $ratingSum = 0;

            foreach ($employeeRatings as $rating) {
                $ratingSum = $ratingSum + $rating->score;
            }

            $data[$i] = $employee;

            $data[$i]['rating'] = ($employeeRatings->count() == 0) ?
                0 : $ratingSum/$employeeRatings->count();

            $data[$i]['activity'] = $employee->activities()->get();
        }

        $result = [
            'message' => 'Success',
            'count' => count($data),
            'data' => $data,
        ];

        return response()->json($result, 200);
    }

    /**
     * @SWG\Get(
     *   path="/employee/{nip}",
     *   summary="Return an employee by nip",
     *   tags={"Employee"},
     *   @SWG\Parameter(
     *     name="nip",
     *     in="path",
     *     description="nip of employee",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="OK"
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="NOT FOUND"
     *   )
     * )
     */
    public function show($nip)
    {
        $employee = Employee::where('nip','=', $nip)
            ->whereNull('deleted_at')
            ->firstOrFail();

        $employeeRatings = Rating::where('id_employee_receiver', '=', $employee->nip)
            ->whereNull('deleted_at')
            ->get();

        $ratingSum = 0;

        foreach ($employeeRatings as $rating) {
            $ratingSum = $ratingSum + $rating->score;
        }

        $data = $employee;

        $data['rating'] = ($employeeRatings->count() == 0) ?
                0 : $ratingSum/$employeeRatings->count();

        $result = [
            'message' => 'Success',
            'count' => count($data),
            'data' => $data,
        ];

        return response()->json($result, 200);
    }

    /**
     * @SWG\Post(
     *   path="/employee",
     *   summary="Create new employee",
     *   tags={"Employee"},
     *   @SWG\Parameter(
     *     name="nip",
     *     in="formData",
     *     description="nip of employee",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="name of employee",
     *     required=true,
     *     type="string"
     *   ),
     *   @SWG\Response(
     *     response=201,
     *     description="CREATED"
     *   )
     * )
     */
    public function create(Request $request)
    {
        $employee = new Employee();

        $employee->nip = $request->nip;
        $employee->name = $request->name;
        $employee->save();

        $result = [
            'message' => 'Created',
        ];

        return response()->json($result, 201);
    }

    /**
     * @SWG\Put(
     *   path="/employee/{nip}",
     *   summary="update an employee by nip",
     *   tags={"Employee"},
     *   @SWG\Parameter(
     *     name="nip",
     *     in="path",
     *     description="nip of employee",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="nip",
     *     in="formData",
     *     description="nip of employee",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="name",
     *     in="formData",
     *     description="employee's name",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="rating",
     *     in="formData",
     *     description="employee's rating",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Response(
     *     response=204,
     *     description="UPDATED"
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="NOT FOUND"
     *   )
     * )
     */
    public function update(Request $request, $nip)
    {
        $employee = Employee::findOrFail($nip);

        $employee->nip = $request->has('nip') ? $request->input('nip') : $employee->nip;
        $employee->name = $request->has('name') ? $request->input('name') : $employee->name;
        $employee->rating = $request->has('rating') ? $request->input('rating') : $employee->rating;
        $employee->save();

        return response()->json([], 204);
    }

    /**
     * @SWG\Delete(
     *   path="/employee/{nip}",
     *   summary="delete an employee by nip",
     *   tags={"Employee"},
     *   @SWG\Parameter(
     *     name="nip",
     *     in="path",
     *     description="id of employee",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(
     *     response=204,
     *     description="DELETED"
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="NOT FOUND"
     *   )
     * )
     */
    public function destroy($nip)
    {
        $employee = Employee::findOrFail($nip);
        $employee->delete();

        return response()->json([], 204);
    }
}

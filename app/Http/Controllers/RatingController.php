<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Employee;
use App\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
    }

    /**
     * @SWG\Get(
     *   path="/ratings",
     *   summary="Return a list of ratings",
     *   tags={"Rating"},
     *   @SWG\Parameter(
     *     name="id_receiver",
     *     in="query",
     *     description="receiver's nip",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="id_sender",
     *     in="query",
     *     description="sender's nip",
     *     required=false,
     *     type="integer"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="OK"
     *   )
     * )
     */
    public function index(Request $request)
    {
        if ($request->has('id_receiver') && $request->has('id_sender')) {
            $data = Rating::where([
                ['id_employee_receiver', '=', $request->get('id_receiver')],
                ['id_employee_sender', '=', $request->get('id_sender')]
            ]);
        } else if ($request->has('id_receiver')) {
            $data = Rating::where('id_employee_receiver', '=', $request->get('id_receiver'));
        } else if ($request->has('id_sender')) {
            $data = Rating::where('id_employee_sender', '=', $request->get('id_sender'));
        }

        $data = ($request->has('_token')) ?
            $data->whereNull('deleted_at')->get() :
            Rating::whereNull('deleted_at')->get();

        $result = [
            'message' => 'Success',
            'count' => count($data),
            'data' => $data,
        ];

        return response()->json($result, 200);
    }

    /**
     * @SWG\Get(
     *   path="/rating/{id}",
     *   summary="Return a rating by id",
     *   tags={"Rating"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="rating's id",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(
     *     response=200,
     *     description="OK"
     *   ),
     *   @SWG\Response(
     *     response=404,
     *     description="NOT FOUND"
     *   )
     * )
     */
    public function show(Request $request, $id = null)
    {
        $data = Rating::findOrFail($id)->first();

        $result = [
            'message' => 'Success',
            'count' => count($data),
            'data' => $data,
        ];

        return response()->json($result, 200);
    }

    /**
     * @SWG\Post(
     *   path="/rating",
     *   summary="Create new rating",
     *   tags={"Rating"},
     *   @SWG\Parameter(
     *     name="score",
     *     in="formData",
     *     description="Score of rating (1-5)",
     *     required=true,
     *     type="integer",
     *     minimum=1,
     *     maximum=5
     *   ),
     *   @SWG\Parameter(
     *     name="desc",
     *     in="formData",
     *     description="Description of rating",
     *     required=false,
     *     type="string"
     *   ),
     *   @SWG\Parameter(
     *     name="id_sender",
     *     in="formData",
     *     description="Sender's NIP",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="id_receiver",
     *     in="formData",
     *     description="Receiver's NIP",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Parameter(
     *     name="id_activity",
     *     in="formData",
     *     description="Activity ID",
     *     required=true,
     *     type="integer"
     *   ),
     *   @SWG\Response(
     *     response=201,
     *     description="CREATED"
     *   )
     * )
     */
    public function create(Request $request)
    {
        $sender = Employee::find($request->id_sender);
        $receiver = Employee::find($request->id_receiver);
        $activity = Activity::find($request->id_activity);

        if ($sender == null)
            return response()->json($result = [
                'message' => 'ID Sender not found',
            ], 400);

        if ($receiver == null)
            return response()->json($result = [
                'message' => 'ID Receiver not found',
            ], 400);

        if ($activity == null)
            return response()->json($result = [
                'message' => 'ID Activity not found',
            ], 400);

        $rating = new Rating();

        $rating->score = $request->score;
        $rating->desc = $request->desc;
        $rating->id_employee_sender = $request->id_sender;
        $rating->id_employee_receiver = $request->id_receiver;
        $rating->id_activity = $request->id_activity;
        $rating->save();

        $result = [
            'message' => 'Created',
        ];

        return response()->json($result, 201);
    }
}

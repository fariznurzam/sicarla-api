<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rating extends Model
{
    use SoftDeletes;

    protected $table = 'ratings';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'score', 'desc', 'id_employee_receiver', 'id_employee_sender'
    ];

    public function sender()
    {
        return $this->belongsTo('App\Employee', 'id_employee_sender', 'nip');
    }

    public function receiver()
    {
        return $this->belongsTo('App\Employee', 'id_employee_receiver', 'nip');
    }

    public function activity()
    {
        return $this->belongsTo('App\Activity', 'id_activity', 'id');
    }
}

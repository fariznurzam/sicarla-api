<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Employee extends Model
{
    use SoftDeletes;

    protected $table = 'employees';
    protected $dates = ['deleted_at'];
    protected $primaryKey = 'nip';
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nip', 'name',
    ];

    public function projects()
    {
        return $this->hasMany('App\Project', 'id_employee', 'nip');
    }

    public function activities()
    {
        return $this->belongsToMany('App\Activity', 'activity_employee',
            'id_employee', 'id_activity')
            ->withPivot('status');
    }

    public function hasReceiveRatings()
    {
        return $this->hasMany('App\Rating', 'id_employee_receiver', 'nip');
    }

    public function hasSendRatings()
    {
        return $this->hasMany('App\Rating', 'id_employee_sender', 'nip');
    }
}

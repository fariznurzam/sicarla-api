<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity extends Model
{
    use SoftDeletes;

    protected $table = 'activities';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name', 'id_project'
    ];

    public function project()
    {
        return $this->belongsTo('App\Project', 'id_project', 'id');
    }

    public function employees()
    {
        return $this->belongsToMany('App\Employee', 'activity_employee',
            'id_activity', 'id_employee')
            ->withPivot('status');
    }

    public function ratings()
    {
        return $this->hasMany('App\Rating', 'id_activity', 'id');
    }
}

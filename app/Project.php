<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    use SoftDeletes;

    protected $table = 'projects';
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'name', 'desc', 'id_employee'
    ];

    public function employee()
    {
        return $this->belongsTo('App\Employee', 'id_employee', 'nip');
    }

    public function activities()
    {
        return $this->hasMany('App\Activity', 'id_project', 'id');
    }
}

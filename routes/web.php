<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1'], function () use ($router) {
    $router->get('/employees', 'EmployeeController@index');
    $router->get('/employee/{nip}', 'EmployeeController@show');
    $router->post('/employee', 'EmployeeController@create');
    $router->put('/employee/{nip}', 'EmployeeController@update');
    $router->delete('/employee/{nip}', 'EmployeeController@destroy');

    $router->get('/projects', 'ProjectController@index');
    $router->get('/project/{id}', 'ProjectController@show');
    $router->post('/project', 'ProjectController@create');

    $router->get('/activities', 'ActivityController@index');
    $router->get('/activity/{id}', 'ActivityController@show');
    $router->post('/activity', 'ActivityController@create');

    $router->get('/activities_employee', 'ActivityEmployeeController@index');
    $router->get('/activity_employee/{id}', 'ActivityEmployeeController@show');
    $router->post('/activity_employee', 'ActivityEmployeeController@create');

    $router->get('/ratings', 'RatingController@index');
    $router->get('/rating/{id}', 'RatingController@show');
    $router->post('/rating', 'RatingController@create');

    $router->get('/portofolio/{id}', 'PortofolioController@show');
    $router->get('/coba', 'EmployeeController@coba');
});

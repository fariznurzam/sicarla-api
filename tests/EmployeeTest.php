<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class EmployeeTest extends TestCase
{
    use DatabaseMigrations;

    protected $parameter;

    public function setUp()
    {
        parent::setUp();

        $this->parameter = [
            'nip' => 100,
            'name' => 'foo'
        ];

        factory('App\Employee')->create($this->parameter);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->get('/');

        $this->assertEquals(
            $this->app->version(), $this->response->getContent()
        );
    }

    public function testExpectReturnAllEmployees()
    {
        $this->get(sprintf('%s/api/v1/employees', $this->baseUrl), []);

        $this->seeStatusCode(200);

        $this->seeJsonStructure([
            'message',
            'count',
            'data' => ['*' =>
                [
                    'nip',
                    'name',
                    'rating',
                    'created_at',
                    'updated_at',
                    'deleted_at'
                ]
            ]
        ]);
    }

    public function testExpectGetSingleEmployee()
    {
        $this->get(sprintf('%s/api/v1/employee/100', $this->baseUrl), []);

        $this->seeStatusCode(200);

        $this->seeJsonStructure([
            'message',
            'count',
            'data' =>
                [
                    'nip',
                    'name',
                    'rating'
                ]

        ]);

        $this->assertContains("foo", $this->response->getContent());

        $this->get(sprintf('%s/api/v1/employee/200', $this->baseUrl), []);

        $this->seeStatusCode(404);
    }

    public function testExpectCreateAnEmployee()
    {
        $this->parameter = [
            'nip' => 200,
            'name' => 'John'
        ];

        $this->post(sprintf('%s/api/v1/employee', $this->baseUrl), $this->parameter, []);

        $this->seeStatusCode(201);

        $this->seeInDatabase('employees', $this->parameter);
    }

    public function testExpectUpdateAnEmployeeName()
    {
        $parameter = [
            'name' => 'bar'
        ];

        $this->put(sprintf('%s/api/v1/employee/100', $this->baseUrl), $parameter, []);

        $this->seeStatusCode(204);

        $this->seeInDatabase('employees', $parameter);

        $this->put(sprintf('%s/api/v1/employee/10', $this->baseUrl), $parameter, []);

        $this->seeStatusCode(404);
    }

    public function testExpectDeleteAnEmployee()
    {
        $this->delete(sprintf('%s/api/v1/employee/100', $this->baseUrl), []);

        $this->assertEquals($this->response->getContent(), '');

        $this->seeStatusCode(204);

        $this->seeInDatabase('employees', ['nip' => 100]);

        $this->delete(sprintf('%s/api/v1/employee/100', $this->baseUrl), []);

        $this->seeStatusCode(404);
    }

    public function tearDown()
    {
        parent::tearDown();
    }
}
